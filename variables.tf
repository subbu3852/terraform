variable "ec2_image" {
  default = "ami-04505e74c0741db8d"
}

variable "ec2_instance_type" {
  default = "t2.micro"
}



variable "ec2_tags" {
  default = "Demo-Terraform-1"
}

variable "ec2_count" {
  default = "2"
}
